// Задание
// Создать поле для ввода цены с валидацией.
//
//     Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.


    // В папке img лежат примеры реализации поля ввода и создающегося span.

let putInput = document.getElementById('input');

console.dir(putInput);
const inputChange = () => {
    putInput.classList.add('btm');
};
const removeInputColor  = (event) => {
    putInput.classList.remove('btm');

    if (event.target.value>0) {
        let divContainer  = document.getElementById('text');
        let span = document.createElement(`span`);
        span.innerText = `Текущая цена: ${event.target.value}`;
        let div = document.createElement('span');
        div.innerText = `x`;
        div.style.width = `20px`;
        div.style.height = `20px`;
        span.appendChild(div);
        divContainer.insertBefore(span, putInput);
        div.onclick = () => {
            // divContainer.style = `display:none`
            span.remove();
            putInput.value = '';
        }
    } else {
        putInput.style.borderColor = 'red';
        let creatText = document.createTextNode('Please enter correct price.');
        putInput.appendChild(creatText);
    }
};

putInput.addEventListener('focus', inputChange);
putInput.addEventListener('blur', removeInputColor);







